package com.rdutta.solutionhealthcheck.solutionhealthcheck.SolutionsValidation;

import com.rdutta.solutionhealthcheck.solutionhealthcheck.EmailFeature.Email;
import com.rdutta.solutionhealthcheck.solutionhealthcheck.Messages.Messages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Duration;
import java.util.*;

@Component
@PropertySource("classpath:server.properties")
public class Solution {

	private static Messages messages = new Messages();

	private final static List<String> excludedSolutionNames = Arrays.asList("OpenTextEntityIdentityComponents",
			"OpenTextEntityTaskOutcomeComponents", "OpenTextEntityWorkflowTemplateComponents",
			"OpenTextInboxTaskManagement");

	public static void adminConsole(int index, String domain, String OTDSUSER, String OTDSPASS, String userNameTo,
			String userNameFrom, String Emailpass) throws IOException, InterruptedException {
		String[] org = { "nimbus" };
		String Content = "";

		System.setProperty("webdriver.chrome.driver",
				"C:/Users/rdutta/Documents/Meta-CourseEra/chromedriver_win32/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--remote-allow-origins=*");
		WebDriver driver = new ChromeDriver(options);

		driver.get(domain + "/home/" + org[index]);
		Thread.sleep(10000);
		driver.findElement(By.id("otds_username")).sendKeys(OTDSUSER);
		driver.findElement(By.id("otds_password")).sendKeys(OTDSPASS);
		driver.findElement(By.id("loginbutton")).click();
		String adminUrl = domain + "/home/" + org[index] + "/app/admin";

		driver.get(adminUrl);
		String currentUrl = driver.getCurrentUrl();

		if (currentUrl.equals(adminUrl)) {
			System.out.println(messages.getSUCCESS());
			System.out.println("==============****==============");
			System.out.println("==============****==============");

			if (domain.equals("https://dmhc-aw2-dev2.opentext.cloud")) {
				WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(100));
				List<WebElement> solutionElements = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(
						By.xpath("//div[@class=\"list-group solutionsPanel\"]/div[@class=\"solution\"]")));

				System.out.printf("\n\n%d elements found in Dev2.\n\n", solutionElements.size());

				List<String> solutionNames = new ArrayList<>();
				Set<String> uniqueSolutionNames = new LinkedHashSet<>();

				if (solutionElements.size() != 0) {
					for (WebElement solutionElement1 : solutionElements) {
						String solutionName1 = solutionElement1.findElement(By.xpath("./a")).getText();
						String solutionStatus = solutionElement1.findElement(By.cssSelector("img.status-image"))
								.getAttribute("title");
						if (solutionStatus.equals(messages.getACTIVE())
								&& !excludedSolutionNames.contains(solutionName1)) {
							solutionNames.add(solutionName1);
							uniqueSolutionNames.add(solutionName1);
						}
						Content = "Solution from "+org[0]+" are avaialable...!!";
					}
					
					
					
					Email email = new Email();
					email.sendMessage(Content, uniqueSolutionNames, "Dev2 Server", userNameTo, userNameFrom, Emailpass);
				} else {
					System.out.println("Please provide a valid domain...!!");
				}

//                adminConsole(index, domain, OTDSUSER, OTDSPASS, userNameTo, userNameFrom, Emailpass);
				Thread.sleep(1000);
				driver.quit();

			}
		}
	}
}
