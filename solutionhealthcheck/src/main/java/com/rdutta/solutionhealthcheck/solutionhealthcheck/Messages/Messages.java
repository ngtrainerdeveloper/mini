package com.rdutta.solutionhealthcheck.solutionhealthcheck.Messages;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Messages {
    private Date date = new Date();

    private final String SUCCESS = "Successfully LoggedIn"+date;
    private final String FAILED = "Failed LoggedIn"+date;
    private final String ACTIVE = "The solution is active.";
    private final String ERRORS = "Solution has errors.";
    private final String WARNING = "Solution has warnings.";

    private String EMAIL_SUCCESS="Report shared successfully to the looped email AMS Group!";
}
