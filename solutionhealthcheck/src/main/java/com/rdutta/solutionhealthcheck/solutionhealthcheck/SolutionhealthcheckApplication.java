package com.rdutta.solutionhealthcheck.solutionhealthcheck;

import com.rdutta.solutionhealthcheck.solutionhealthcheck.SolutionsValidation.Solution;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;

@SpringBootApplication
public class SolutionhealthcheckApplication {


	public static void main(String[] args) throws IOException, InterruptedException {
		SpringApplication.run(SolutionhealthcheckApplication.class, args);
		int index = 0;
		String domain = args[1];
		String OTDSUSER = args[2]+"@otds.admin";
		String OTDSPASS = args[3];
		String userNameTo = args[4]+"@opentext.com";
		String userNameFrom = args[5]+"@opentext.com";
		String Emailpass = args[6];

		String passOTDS = OTDSPASS;

		Solution solution = new Solution();
		solution.adminConsole(index, domain, OTDSUSER, passOTDS, userNameTo, userNameFrom, Emailpass);
	}

}
