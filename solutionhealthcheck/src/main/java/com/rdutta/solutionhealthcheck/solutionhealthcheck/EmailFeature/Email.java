package com.rdutta.solutionhealthcheck.solutionhealthcheck.EmailFeature;

import com.rdutta.solutionhealthcheck.solutionhealthcheck.Messages.Messages;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

@Configuration
@Component
public class Email {
    public void sendMessage(String content, Set<String> tableData, String server_name, String userNameTo, String userNameFrom, String emailpass) throws IOException{
        Properties serverlist_properties = new Properties();
        FileInputStream input1 = new FileInputStream(
                "C:/Users/rdutta/Videos/documentation/solutionhealthcheck/src/main/resources/server.properties");
        serverlist_properties.load(input1);


        String host = serverlist_properties.getProperty("SMTP_HOST");
        final String user = serverlist_properties.getProperty("SMTP_SRCMAILADDR");
        final String password = serverlist_properties.getProperty("SMTP_SRCMAILPWD");
        String to = serverlist_properties.getProperty("SMTP_RECMAILADDR");

        //Get the session Object
        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.auth", "true");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(user, password);
                    }
                });

        try {
            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress(user));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            message.setSubject("DMHC Solution Healthcheck "+server_name);

            MimeBodyPart messageBodyPart = new MimeBodyPart();

            if (tableData.size() != 0) {
                // Create the message part
                String htmlContent = "<html><head><style>" +
                        "table {border-collapse: collapse; width: 100%; border: 1px solid #dee2e6;}" +
                        "th, td {text-align: left; padding: 8px;}" +
                        "th {background-color: #91a7ff; color: #212529; font-size: 16px;}" +
                        "tr:nth-child(even){background-color: #dee2e6}" +
                        ".mat-table { border-collapse: collapse; }" +
                        ".mat-header-cell { background-color: #1976d2; color: #212529; font-weight: bold; font-size: 16px; }"
                        +
                        ".mat-cell { padding: 8px; font-size: 14px; }" +
                        ".mat-row:nth-child(even) { background-color: #f2f2f2; }" +
                        ".table-heading { font-size: 18px; font-weight: bold; margin-bottom: 20px; color: #212529; }" +
                        "</style></head><body>" +
                        "<div class='table-heading'>Solution Availibility Status Report</div>";
                String heading = content;
                htmlContent += heading;
                htmlContent += "<table><tr><th>Solution</th><th>Status</th></tr>";
                for (String solution_name : tableData) {
                    htmlContent += "<tr class='mat-row'><td class='mat-cell' style='border: 1px solid #dee2e6;'>"
                            + solution_name
                            + "</td><td class='mat-cell' style='border: 1px solid #dee2e6; color: #63e6be; font-size: 18px; font-weight: bold;'>AVAILABLE <span style='font-size: 14px;'>&#10004;</span></td></tr>";
                }



                messageBodyPart.setContent(htmlContent, "text/html; charset=utf-8");

            } else {

                String htmlContent = "<span style='font-size: 24px;'>&#10004;</span>";

                String heading = content;
                htmlContent = htmlContent + " " + heading;

                htmlContent += "</table></body></html>";

                messageBodyPart.setContent(htmlContent, "text/html; charset=utf-8");
            }

            // Create a multipart message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            message.setContent(multipart);

            // Send message
            Transport.send(message);

            Messages messages = new Messages();

            System.out.println(messages.getEMAIL_SUCCESS());

        } catch (MessagingException e) {
            throw new RuntimeException(e);
    }
    }
}
